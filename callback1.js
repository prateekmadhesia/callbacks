const fs = require("fs");

function callback(Id, responseFunction) {
  if (responseFunction == undefined || typeof responseFunction != "function") {
    console.error("Invalid callbackFunction");
  } else if (typeof Id == undefined) {
    responseFunction(new Error("Invalid Id"));
  } else {
    setTimeout(() => {
      fs.readFile("./data/boards.json", "utf-8", (err, stringJson) => {
        if (err) {
          responseFunction(err);
        } else {
          try {
            const data = JSON.parse(stringJson);
            if (typeof Id == "string" || typeof Id == "number") {
              if (typeof Id == "number") {
                Id = Id.toString();
              }
              const result = data.filter((element) => {
                return element.id == Id;
              });
              responseFunction(null, result);
            }
          } catch (error) {
            responseFunction(error);
          }
        }
      });
    }, 2 * 1000);
  }
}

module.exports = callback;
