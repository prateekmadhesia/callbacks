const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

const cardInformation = (err, result) => {
  if (err) {
    console.log("Failed");
    console.error(err);
  } else {
    if (result == undefined || result.length == 0) {
      console.error("Card data not found");
    } else {
      console.log("All the cards for the Mind list=");
      console.log(result);
    }
  }
};

const listInformation = (err, result) => {
  if (err) {
    console.log("Failed");
    cardInformation(err);
  } else {
    if (result == undefined || result.length == 0) {
      cardInformation("List Data not found");
    } else {
      console.log("All the lists for the Thanos board=");
      console.log(result);
      const listId = result
        .filter((element) => {
          return element.name == "Mind";
        })
        .map((element) => {
          return element.id;
        });

      callback3(listId, cardInformation);
    }
  }
};

const boardInformation = (err, result) => {
  if (err) {
    console.log("Failed");
    listInformation(err);
  } else {
    console.log("Information from Thanos board=");
    console.log(result);
    if (result == undefined || result.length == 0) {
      listInformation("Board data not found");
    } else {
      let thanosId = result[0].id;
      callback2(thanosId, listInformation);
    }
  }
};

function callback() {
  let thanos = "mcu453ed";
  callback1(thanos, boardInformation);
}

module.exports = callback;
