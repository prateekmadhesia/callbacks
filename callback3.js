const fs = require("fs");

function callback(Id, responseFunction) {
  if (responseFunction == undefined || typeof responseFunction != "function") {
    console.error("Invalid callbackFunction");
  } else if (typeof Id == undefined) {
    responseFunction(new Error("Invalid Id"));
  } else {
    setTimeout(() => {
      fs.readFile("./data/cards.json", "utf-8", (err, stringJson) => {
        if (err) {
          responseFunction(err);
        } else {
          try {
            const data = JSON.parse(stringJson);
            if (typeof Id == "object") {
              const result = Id.reduce((resultArray, currentValue) => {
                if (currentValue in data) {
                  resultArray.push(data[currentValue]);
                }
                return resultArray;
              }, []);
              responseFunction(null, result);
            } else if (typeof Id == "string" || typeof Id == "number") {
              let result;
              if (typeof Id == "number") {
                Id = Id.toString();
              }
              if (Id in data) {
                result = data[Id];
              } else {
                result = [];
              }
              responseFunction(null, result);
            } else {
              responseFunction(null, []);
            }
          } catch (error) {
            responseFunction(error);
          }
        }
      });
    }, 2 * 1000);
  }
}

module.exports = callback;
