const callback = require("../callback3.js");

function innerCallback(err, result) {
  if (err) {
    console.log("Failed");
    console.error(err);
  } else {
    if (result.length == 0) {
      console.log("Data not found");
    } else {
      console.log("Success");
      console.log(result);
    }
  }
}
let id1 = "qwsa221";
let id2 = "jwkh245";

callback(id1, innerCallback);
callback([id1], innerCallback);
callback([id2], innerCallback);
callback([], innerCallback);
callback(1234, innerCallback);