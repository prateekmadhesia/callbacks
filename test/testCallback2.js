const callback = require("../callback2.js");

function innerCallback(err, result) {
  if (err) {
    console.log("Failed");
    console.error(err);
  } else {
    if (result.length == 0) {
      console.log("Data not found");
    } else {
      console.log("Success");
      console.log(result);
    }
  }
}
let id1 = "mcu453ed";
let id2 = "abc122dc";

callback(id1, innerCallback);
callback(id2, innerCallback);
callback("122", innerCallback);
callback(122, innerCallback);